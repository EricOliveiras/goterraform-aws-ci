package main

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestMain(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatalf("Erro ao criar a requisição: %v", err)
	}

	rr := httptest.NewRecorder()

	greet(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler retornou código de status incorreto: esperado %v, obtido %v",
			http.StatusOK, status)
	}

	expectedPrefix := "Hello World!"
	if !strings.HasPrefix(rr.Body.String(), expectedPrefix) {
		t.Errorf("Corpo da resposta não contém a saudação esperada. Esperado prefixo: %s",
			expectedPrefix)
	}
}
