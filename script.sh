#!/bin/bash

sudo apt update
sudo apt upgrade -y

sudo apt install golang-go -y

curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt install gitlab-runner

sudo chown gitlab-runner /home/ubuntu/app/ -R